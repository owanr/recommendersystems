import java.io.*;
import java.util.*;
import java.lang.*;
/*
RecommenderSystems, due 3/9
Written by Brynna Mering and Risako Owan
*/

// Use cosine distance to measure the distance between users or
// items on the values that they actually share in common that are not missing. Normalize user
// ratings by subtracting out each user's average rating.
public class UserRS{
	private static List<HashMap<Integer, Integer>> listOfUserToRatings = new ArrayList<HashMap<Integer, Integer>>();
        //Creates list (index = userID) of dictionaries that map from movieID to ratings
    private static HashMap<Integer, Double> userAverages = new HashMap<Integer, Double>();
    private static HashMap<Integer, List<Double>> movieToRatings = new HashMap<Integer, List<Double>>();
	private static HashMap<Integer, String> movieNames = new HashMap<Integer, String>();
	private static List<HashMap<Integer, Double>> normalizedListOfUserToRatings = new ArrayList<HashMap<Integer, Double>>();
    private static List<Integer> topTen = new ArrayList<Integer>();
    private static HashMap<Integer, Double[]> movieIDtoRatingDiff = new HashMap<Integer, Double[]>();
    private static List<Integer> bottomTen = new ArrayList<Integer>();
	private static int numSimilarUsers = 10;
	private static int minimumCommonMovies = 6;//The number at which each person had at least 10 similar users
    //private static HashMap<Integer, Integer> minimumCommonMoviesTest = new HashMap<Integer, Integer>();
	 
	/*
    ** Reads data from given fileName file
    ** Fills listOfUserToRatings, userAverages (global)
    */
    public static void readFile(String fileName) {
        File dataFile = new File(fileName);
        BufferedReader reader = null;
        listOfUserToRatings.clear();
        userAverages.clear();
        List<Integer> test = new ArrayList<Integer>();
        try {
        	//Initialize and declare variables
            //UserIDs will start at 0, so we can use them interchangeably with indexes
        	Integer currentUser = 0;
            Double sum = 0.0;
            Integer n = 0;

            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            while (line != null && line.length() != 0) {
            	String[] splitLine = line.split("\t");
                HashMap<Integer, Integer> movieRating = new HashMap<Integer, Integer>();
                //While we are getting ratings from the same user, put them in the same movieRating dictionary
                //Data user id | item id | rating | timestamp.
                while (currentUser == (Integer.parseInt(splitLine[0])-1)){
                	Integer movieID = Integer.parseInt(splitLine[1]);
	                Integer rating = Integer.parseInt(splitLine[2]);
	                movieRating.put(movieID,rating);
	                sum += rating;
	                n++;
	                line = reader.readLine();
	                if (line != null){
	                	splitLine = line.split("\t");
	                } else {
	                	break;
	                }
                }
                listOfUserToRatings.add(movieRating);
                userAverages.put(currentUser,(sum/n));
                currentUser++;
                sum = 0.0;
                n = 0;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
            //return listOfUserToRatings;
        }
    }

    /*
    ** Uses listOfUserToRatings and userAverages to normalize ratings
    ** Subtracks average from ratings
    */
    public static void normalizeRatings(){
    	normalizedListOfUserToRatings.clear();
    	Integer userID = 0;
    	for (HashMap<Integer, Integer> eachUserRating : listOfUserToRatings){
    		HashMap<Integer, Double> temp = new HashMap<Integer, Double>();
    		for (Integer movieID : eachUserRating.keySet()){
                Double normalizedRating = eachUserRating.get(movieID)-userAverages.get(userID);
    			temp.put(movieID,normalizedRating);
    		}
    		normalizedListOfUserToRatings.add(temp);
    		userID++;
    	}
    }

    /*
    ** Calculates cosine distance between given user dictionaries.
    ** Gets called in findNSimilarUsers (no need to call in main) 
    */
    public static Double calculateCosine(HashMap<Integer, Double> user1Ratings, HashMap<Integer, Double> user2Ratings){
    	Set<Integer> intersectionOfSeenMovies = new HashSet<Integer>();
    	intersectionOfSeenMovies.addAll(user1Ratings.keySet());
    	intersectionOfSeenMovies.retainAll(user2Ratings.keySet());

    	//Calculates dot product
    	Double dotProduct = 0.0;
    	for (Integer movieID : intersectionOfSeenMovies){
    		//if (user1Ratings.containsKey(movieID) && user2Ratings.containsKey(movieID)){
    		dotProduct += user1Ratings.get(movieID)*user2Ratings.get(movieID);
    		//}
    	}

    	//Calculates user1 vector magnitude
    	Double vectorMagnitude1 = 0.0;
    	for (Integer movieID : user1Ratings.keySet()){
    		vectorMagnitude1 += Math.pow(user1Ratings.get(movieID),2);
    	}
    	vectorMagnitude1 = Math.sqrt(vectorMagnitude1);

    	//Calculates user1 vector magnitude
    	Double vectorMagnitude2 = 0.0;
    	for (Integer movieID : user2Ratings.keySet()){
    		vectorMagnitude2 += Math.pow(user2Ratings.get(movieID),2);
    	}
    	vectorMagnitude2 = Math.sqrt(vectorMagnitude2);

    	return (dotProduct/(vectorMagnitude1*vectorMagnitude2));
    }

    /*
    :* Finds the N (declared as a global variable) most similar users according to their distances
    ** Returns a list of the n most similar users given the givenUser dictionary
    ** Gets called in calculateBestEstimate
    */
    public static List<Integer> findNSimilarUsers(HashMap<Integer, Double> givenUser){
    	List<Integer> similarUserIDs = new ArrayList<Integer>();
		PriorityQueue<UserPair> distPriorityQueue = new PriorityQueue<UserPair>();
		Double dist;
		Integer givenUserID = normalizedListOfUserToRatings.indexOf(givenUser);
        //Integer getMaxMoviesInCommon = 0;
		for (int id = 0; id < normalizedListOfUserToRatings.size(); id++){
			if (id != givenUserID){
				//Checks the number of movies that two users share in common in order to consider them potentially
				//nearest neighbors; if they share too few movies in common, you may want to exclude them
				HashMap<Integer, Double> potentialSimilarUser = normalizedListOfUserToRatings.get(id);
				Set<Integer> moviesInCommon = new HashSet<Integer>();
				moviesInCommon.addAll(givenUser.keySet());
				moviesInCommon.retainAll(potentialSimilarUser.keySet());
                //if(moviesInCommon.size()>getMaxMoviesInCommon){getMaxMoviesInCommon=moviesInCommon.size();}
				if (moviesInCommon.size() >= minimumCommonMovies){
					dist = calculateCosine(givenUser, potentialSimilarUser)-1;//Measures similarity
					UserPair newUserPair = new UserPair(id, givenUserID, Math.pow(dist,2));
	                distPriorityQueue.add(newUserPair);
				}		
			}
		}
		for (int i = 0; i < 50;i++){
            if (!distPriorityQueue.isEmpty()){
    			UserPair neighbor = distPriorityQueue.poll();    
    			similarUserIDs.add(neighbor.getUser1());
            }
		}
        //DEBUGGINGGGGG
        // smallest distance doesn't mean closest average!!
        // while (!distPriorityQueue.isEmpty()){
        //     UserPair neighbor = distPriorityQueue.poll();
        //     System.out.println(userAverages.get(neighbor.getUser1() + 1));
        // }
		return similarUserIDs;
    }

    /*
    ** Returns a dictionary of estimated ratings for all unseen movies of givenUser
    ** Gets called in compareEstimateWithTestDataAndReturnRMSE()
    */
    public static Double calculateBestEstimateForMissingValue(HashMap<Integer, Double> givenUser, Integer missingMovie){
        //Initializing+Declaring all variables for method        
        List<Integer> similarUserIDs = findNSimilarUsers(givenUser);
        Double estimateRating = 0.0;
        //Estimates ratings for movies user hasn't seen yet
        Integer numMoviesToEstimateFrom = 0;
        for (Integer userID : similarUserIDs){
            HashMap<Integer, Double> similarUser = normalizedListOfUserToRatings.get(userID);
            if (similarUser.containsKey(missingMovie)){
                estimateRating += similarUser.get(missingMovie);
                numMoviesToEstimateFrom++;
            }
        }
        //We only have 10 similar uses so let's try to use all the info available.
        if (numMoviesToEstimateFrom > 0) {
            estimateRating = estimateRating/numMoviesToEstimateFrom;
            //estimatedMovieRecommendations.put(i, estimateRating);
        } else {
            //If no info is available, set rating so that you are neutral towards it.
            //estimatedMovieRecommendations.put(i, 0.0);
            estimateRating = 0.0;
        }
        return estimateRating;
        // //Initializing+Declaring all variables for method        
        // HashMap<Integer, Double> estimatedMovieRecommendations = new HashMap<Integer, Double>();
        // List<Integer> similarUserIDs = findNSimilarUsers(givenUser);
        // //Iterates through all movieIDs
        // for (int i = 1; i <= 1682; i++){
        //     //Estimates ratings for movies user hasn't seen yet
        //     if (!givenUser.containsKey(i)){
        //         Double estimateRating = 0.0;
        //         Integer numMoviesToEstimateFrom = 0;

        //         for (Integer userID : similarUserIDs){
        //             HashMap<Integer, Double> similarUser = normalizedListOfUserToRatings.get(userID);
        //             if (similarUser.containsKey(i)){
        //                 estimateRating += similarUser.get(i);
        //                 numMoviesToEstimateFrom++;
        //             }
        //         }
        //         //We only have 10 similar uses so let's try to use all the info available.
        //         if (numMoviesToEstimateFrom > 0) {
        //             estimateRating = estimateRating/numMoviesToEstimateFrom;
        //             estimatedMovieRecommendations.put(i, estimateRating);
        //         } else {
        //             //If no info is available, set rating so that you are neutral towards it.
        //             estimatedMovieRecommendations.put(i, 0.0);
        //         }
        //     }
        // }
        // return estimatedMovieRecommendations;
    }

    /*
    ** Reads values from us.test and compares it to values created in calculateBestEstimateForMissingValue
    **
    */
    public static Double compareEstimateWithTestDataAndReturnRMSE(){
        //Polls smallest first
    	PriorityQueue<MovieEstimate> estimatePriorityQueue = new PriorityQueue<MovieEstimate>();
		File dataFile = new File("ua.test");
        BufferedReader reader = null;
        Double rmse = 0.0;
        //Each user has 10 movies in the test data
        Integer numEstimated = 10*normalizedListOfUserToRatings.size();
        //System.out.println(numEstimated);
        //movieID --> [ratingDiff^2, numberOfUsersRated]
        
        //Double maxTest = 0.0;
        try {
        	Integer currentUser = 0;
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            while (line != null && line.length() != 0) {
                //HashMap<Integer, Double> estimateRatingForGivenUser = calculateBestEstimateForMissingValue(normalizedListOfUserToRatings.get(currentUser));
                //System.out.println("after++"+currentUser);
            	String[] splitLine = line.split("\t");
                while (currentUser == (Integer.parseInt(splitLine[0])-1)){
                	Integer movieID = Integer.parseInt(splitLine[1]);
	                Double normalizedActualRating = Double.parseDouble(splitLine[2])-userAverages.get(currentUser);
                    //Double estimatedRating = estimateRatingForGivenUser.get(movieID);
	                Double estimatedRating = calculateBestEstimateForMissingValue(normalizedListOfUserToRatings.get(currentUser),movieID);
	                Double ratingDiff = normalizedActualRating - estimatedRating;

                    //Keeps track of each movie's ratings and rating differences
                    // List<Double> oldList;
                    // if (movieToRatings.containsKey(movieID)){
                    //     oldList = movieToRatings.get(movieID);
                    // } else {
                    //     oldList = new ArrayList<Double>();
                    // }
                    // oldList.add(ratingDiff);
                    // movieToRatings.put(movieID,oldList);

	                rmse += Math.pow(ratingDiff,2);
                    Double[] movieInfo;
	                if (movieIDtoRatingDiff.containsKey(movieID)){
                        movieInfo = movieIDtoRatingDiff.get(movieID);
                        movieInfo[0] = movieInfo[0] + Math.pow(ratingDiff,2);
                        movieInfo[1] = movieInfo[1] + 1;
                        //if(movieInfo[1]>maxTest){maxTest=movieInfo[1];}
                    } else {
                        movieInfo = new Double[2];
                        movieInfo[0] = Math.pow(ratingDiff,2);
                        movieInfo[1] = 1.0;
                        //if(movieInfo[1]>maxTest){maxTest=movieInfo[1];}
                    }
                    movieIDtoRatingDiff.put(movieID, movieInfo);

	                line = reader.readLine();
	                if (line != null){
	                	splitLine = line.split("\t");
	                } else {
	                	break;    
	                }
                }//end of while-loop
                currentUser++;
                //line = reader.readLine();
            }

            for (Integer movieIDForPQ : movieIDtoRatingDiff.keySet()){
                Double[] ratingDiffInfo = movieIDtoRatingDiff.get(movieIDForPQ);
                //Checking how many times you guessed; ratingDiffInfo[1] = numberOfUsersRated
                //How we chose parameter 30:
                //The max was for common number of users was 88, so we wanted something near 40
                //With 30, the size of the PQ becomes 39, so getting the top/bottom 10 gives us the first quartile and last quartile
                if (ratingDiffInfo[1] > 30){
                    MovieEstimate movieEstDiff = new MovieEstimate(movieIDForPQ, ratingDiffInfo[0]/ratingDiffInfo[1]);
                    estimatePriorityQueue.add(movieEstDiff);
                }
            }
            //System.out.println(estimatePriorityQueue.size()+"**");

            rmse = Math.sqrt((rmse/numEstimated));
           
            //Converting queue to array to get the 10 best and 10 worst
            MovieEstimate[] queueArray = new MovieEstimate[estimatePriorityQueue.size()];
            queueArray = estimatePriorityQueue.toArray(queueArray);
            //Gets the top 10 best and worst estimates
            for (int i = 0; i < 10; i++){
                topTen.add(queueArray[i].getMovieID());
                bottomTen.add(queueArray[estimatePriorityQueue.size()-1-i].getMovieID());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
            return rmse;
        }
    }

    public static void readMovieNames(){
        File dataFile = new File("u.item");
        BufferedReader reader = null;
        movieNames.clear();
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();

            while (line != null && line.length() != 0) {
                String[] splitLine = line.split("\\|");
                //Insert movieID --> movieName
                movieNames.put(Integer.parseInt(splitLine[0]), splitLine[1]);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    public static List<String> getMovieNamesAndRatingDiff(List<Integer> movieIDList){
        List<String> listOfNames = new ArrayList<String>();
        for (Integer movieID : movieIDList){
            listOfNames.add(movieNames.get(movieID) + " with avg. diff " + movieIDtoRatingDiff.get(movieID)[0]/movieIDtoRatingDiff.get(movieID)[1]);
        }
        return listOfNames;
    }

	public static void main(String[] args){
        //Retrieves all necessary info
		readFile("ua.base");
        readMovieNames();

        //Normalizes all data
		normalizeRatings();
        final long startTime = System.currentTimeMillis();
        
        
        Double rmse = compareEstimateWithTestDataAndReturnRMSE();
        final long endTime = System.currentTimeMillis();
        System.out.println("Total execution time: " + (endTime - startTime)/1000);
        System.out.println("RMSE:"+rmse);
        System.out.println("Top 10: "+getMovieNamesAndRatingDiff(topTen));
        System.out.println("Bottom 10: "+getMovieNamesAndRatingDiff(bottomTen));
        //findNSimilarUsers(normalizedListOfUserToRatings.get(4));
        //System.out.println(normalizedListOfUserToRatings.get(0));
        //findNSimilarUsers(normalizedListOfUserToRatings.get(3));
		// System.out.println("neightbors"+findNSimilarUsers(normalizedListOfUserToRatings.get(3)));
  //       System.out.println(userAverages.get(4)+"-----");
  //       for (Integer id : findNSimilarUsers(normalizedListOfUserToRatings.get(3))){
  //           System.out.print(userAverages.get(id)+", ");
  //       }
  //       findNSimilarUsers(normalizedListOfUserToRatings.get(0));
		// calculateBestEstimateForMissingValue(normalizedListOfUserToRatings.get(0));
		

        //System.out.println(normalizedListOfUserToRatings.get(0));
		//System.out.println(userAverages);
	}
}