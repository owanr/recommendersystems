import java.io.*;
import java.util.*;
/**
* ItemPair.java is a class which stores the distance between two priority queues
* It extends comparable by comparing distance.
*
* @author Brynna Mering
*/
public class ItemPair implements Comparable<ItemPair>{
	private Integer item1;
	private Integer item2;
	private Double distance;

	/**
	* Constructor for ClusterPair
	*/
	public ItemPair(Integer i1, Integer i2, Double distance){
		this.distance = distance;
		this.item1 = i1;
		this.item2 = i2;
	}

	/**
	* Constructor for ClusterPair
	*/
	public ItemPair(ItemPair itemPair){
		this.distance = itemPair.getDistance();
		this.item1 = itemPair.getItem1();
		this.item2 = itemPair.getItem2();
	}

	public Double getDistance(){
		return this.distance;
	}

	public Integer getItem1(){
		return this.item1;
	}

	public Integer getItem2(){
		return this.item2;
	}

	/**
	* compareTo() allows this ClusterPair to be compared to another ClusterPair based on distance
	*/

	public int compareTo(ItemPair compareItemPair){
		if(compareItemPair.getDistance()>this.distance){
			return -1;
		} else if(compareItemPair.getDistance() == this.distance){
			return 0;
		} else {
			return 1;
		}
	}
}