import java.io.*;
import java.util.*;
import java.lang.*;
/*
ItemRS, due 3/9
Written by Brynna Mering and Risako Owan
*/

public class ItemRS{
	//Dictionary of movie id -> set of users who have rated the movie
	private static HashMap<Integer, Set<Integer>> mapOfMoviesToUsers = new HashMap<Integer, Set<Integer>>();
	//List of Dictionaries of movieId -> rating. The index of the dictionary is the user.
	private static List<HashMap<Integer, Integer>> listOfUserToRatings = new ArrayList<HashMap<Integer, Integer>>();
	//Dictionary containing the average rating for each movie
	private static HashMap<Integer, Double> movieAverages = new HashMap<Integer, Double>();
	//Dictionary containing the average rating given by each user
	private static HashMap<Integer, Double> userAverages = new HashMap<Integer, Double>();
	//List of Dictionaries of movieId -> normalized rating. The index of the dictionary is the user.
	private static List<HashMap<Integer, Double>> normalizedListOfUserToRatings = new ArrayList<HashMap<Integer, Double>>();

	private static HashMap<Integer, String> movieNames = new HashMap<Integer, String>();

	private static HashMap<Set<Integer>, Double> movieDistances = new HashMap<Set<Integer>, Double>();

	private static List<Integer> topTen = new ArrayList<Integer>();
	private static List<Integer> bottomTen = new ArrayList<Integer>();
	private static int numSimilarMovies = 10;
	private static Double goodMovieRating = 3.5;

	/*
    ** Reads data from file
    ** Fills listOfUserToRatings and userAverages (global variables)
    */
    public static void readFile(String fileName) {
        File dataFile = new File(fileName);
        BufferedReader reader = null;
        listOfUserToRatings.clear();
        userAverages.clear();
        try {
        	//Initialize and declare variables
        	Integer currentUser = 0;
            Double sum = 0.0;
            Integer n = 0;

            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            while (line != null && line.length() != 0) {
            	String[] splitLine = line.split("\t");
                HashMap<Integer, Integer> movieRating = new HashMap<Integer, Integer>();
                //While we are getting ratings from the same user, put them in the same movieRating dictionary
                //Data user id | item id | rating | timestamp.
                while (currentUser == Integer.parseInt(splitLine[0])-1){
                	Integer movieID = Integer.parseInt(splitLine[1]);
	                Integer rating = Integer.parseInt(splitLine[2]);
	                movieRating.put(movieID,rating);
	                sum += rating;
	                n++;
	                line = reader.readLine();
	                if (line != null){
	                	splitLine = line.split("\t");
	                } else {
	                	break;
	                }
                }
                listOfUserToRatings.add(movieRating);
                userAverages.put(currentUser,(sum/n));
                currentUser++;
                sum = 0.0;
                n = 0;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

	/*
    ** Fills mapOfMoviesToUsers
    */
    public static void fillMoviesToUsers(){
    	normalizeRatings();
    	mapOfMoviesToUsers = new HashMap<Integer, Set<Integer>>();
    	Set<Integer> users = new HashSet<Integer>();
    	//Loop through all users and for each movie that they rate 
    	//add that user to mapOfMoviesToUsers
    	for(int i = 0; i<listOfUserToRatings.size(); i++){
    		users.clear();
    		HashMap<Integer, Integer> user = new HashMap<Integer, Integer>(listOfUserToRatings.get(i));
    		for(Integer movie : user.keySet()){
    			if(mapOfMoviesToUsers.containsKey(movie)){
    				users = new HashSet<Integer>(mapOfMoviesToUsers.get(movie));
    				users.add(i);

    			} else{
    				users = new HashSet<Integer>();
    				users.add(i);
    			}
    			mapOfMoviesToUsers.remove(movie);
    			mapOfMoviesToUsers.put(movie, users);
    		}
    	}
    }


	/*
    ** Finds the average rating for each movie and stores it in movieAverages
    */
    public static void getMovieAverages(){
    	for(Integer movie : mapOfMoviesToUsers.keySet()){
    		Double movieAvg = 0.0;
    		int count = 0;
    		Set<Integer> users = new HashSet<Integer>(mapOfMoviesToUsers.get(movie));
    		for(Integer user : users){
    			movieAvg += normalizedListOfUserToRatings.get(user).get(movie);
    			count++;
    		}
    		movieAvg = movieAvg/count;
    		movieAverages.put(movie, movieAvg);
    	}
    }

	/*
    ** Normalizes the ratings and stores them in 
    ** normalizedListOfUserToRatings
    */
    public static void normalizeRatings(){
    	normalizedListOfUserToRatings.clear();
    	Integer userID = 0;
    	for (HashMap<Integer, Integer> eachUserRating : listOfUserToRatings){
    		HashMap<Integer, Double> temp = new HashMap<Integer, Double>();
    		for (Integer movieID : eachUserRating.keySet()){
    			temp.put(movieID,eachUserRating.get(movieID)-userAverages.get(userID));
    		}
    		normalizedListOfUserToRatings.add(temp);
    		userID++;
    	}
    }

    /*
    ** Calculates the similarity between two movies
    ** and returns a Double
    */
    public static Double calculateItemSimilarity(Integer movie1, Integer movie2){
    	Set<Integer> commonUsers = new HashSet<Integer>();
    	if(mapOfMoviesToUsers.get(movie1) != null && mapOfMoviesToUsers.get(movie2) != null){
    		commonUsers.addAll(mapOfMoviesToUsers.get(movie1));
    		commonUsers.retainAll(mapOfMoviesToUsers.get(movie2));
    	} 
    	
    	List<Double> movie1Ratings = new ArrayList<Double>();
    	List<Double> movie2Ratings = new ArrayList<Double>();
    	if(commonUsers.size()>0){
	    	for(Integer user : commonUsers){
	    		//System.out.println("line 156");
	    		HashMap<Integer, Double> userMap = new HashMap<Integer, Double>(normalizedListOfUserToRatings.get(user));
	    		movie1Ratings.add(userMap.get(movie1));
	    		movie2Ratings.add(userMap.get(movie2));
	    	}

	    	//Calculates dot product
	    	Double dotProduct = 0.0;
	    	Double vectorMagnitude1 = 0.0;
	    	Double vectorMagnitude2 = 0.0;
	    	for (int i = 0; i < commonUsers.size(); i++){
	    		dotProduct += movie1Ratings.get(i)*movie2Ratings.get(i);
	    		vectorMagnitude1 += Math.pow(movie1Ratings.get(i),2);
	    		vectorMagnitude2 += Math.pow(movie2Ratings.get(i),2);
	    	}
	    	vectorMagnitude1 = Math.sqrt(vectorMagnitude1);
	    	vectorMagnitude2 = Math.sqrt(vectorMagnitude2);

	    	return (dotProduct/(vectorMagnitude1*vectorMagnitude2));
	    } else{
	    	return Double.MAX_VALUE;
	    }
    }
    public static void calculateAllMovieDistances(){
    	Set<Integer> pair = new HashSet<Integer>();
    	for(Integer i = 1; i <=mapOfMoviesToUsers.keySet().size(); i++){
    		for(Integer j = i+1; j <=mapOfMoviesToUsers.keySet().size(); j++){
    			Double similarity = calculateItemSimilarity(i, j);
    			pair.clear();
    			pair.add(i);
    			pair.add(j);
    			movieDistances.put(pair, similarity);
    		}
    	}
    }

	/*
    ** Normalizes the ratings and stores them in 
    ** normalizedListOfUserToRatings
    */
    public static Double predictMissingMovieRating(HashMap<Integer, Double> user, Integer missingMovie){
    	PriorityQueue<ItemPair> similarMovieQueue = new PriorityQueue<ItemPair>();
    	for(Integer seenMovie : user.keySet()){
    		Double similarity = calculateItemSimilarity(missingMovie, seenMovie);
    		ItemPair itemPair = new ItemPair(missingMovie, seenMovie, similarity);
    		similarMovieQueue.add(itemPair);
    	}
    	Double avgRating = 0.0;
    	for(int k = 0; k < numSimilarMovies; k++){
    		ItemPair tempIP = new ItemPair(similarMovieQueue.poll());
    		Integer seenMovie = tempIP.getItem2();
    		avgRating += user.get(seenMovie);
    	}
    	avgRating = avgRating/numSimilarMovies;
    	return avgRating;		
    }

    public static Double getRMSE(){
        //Polls smallest first
    	PriorityQueue<MovieEstimate> estimatePriorityQueue = new PriorityQueue<MovieEstimate>();
		File dataFile = new File("ua.test");
        BufferedReader reader = null;
        Double rmse = 0.0;
        List<Integer> missingMovies = new ArrayList<Integer>();
        //Each user has 10 movies in the test data
        Integer numEstimated = 10*normalizedListOfUserToRatings.size();
        try {
        	Integer currentUser = 0;
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            while (line != null && line.length() != 0) {

            	String[] splitLine = line.split("\t");
            	missingMovies.clear();
                while (currentUser.equals(Integer.parseInt(splitLine[0])-1)){
                	Integer movieID = Integer.parseInt(splitLine[1]);
                	missingMovies.add(movieID);

	                Double normalizedActualRating = Double.parseDouble(splitLine[2])-userAverages.get(currentUser);

	                Double estimatedRating = predictMissingMovieRating(normalizedListOfUserToRatings.get(currentUser), movieID);
	                Double ratingDiff = normalizedActualRating - estimatedRating;
	                rmse += Math.pow(ratingDiff,2);
	                MovieEstimate movieEstDiff = new MovieEstimate(movieID, Math.pow(ratingDiff,2));
	                estimatePriorityQueue.add(movieEstDiff);
	                line = reader.readLine();
	                if (line != null){
	                	splitLine = line.split("\t");
	                } else{
	                	break;
	                }

                }//end of while-loop
                currentUser++;
            }
            rmse = Math.sqrt((rmse/numEstimated));
           
            //Converting queue to array to get the 10 best and 10 worst
            MovieEstimate[] queueArray = new MovieEstimate[estimatePriorityQueue.size()];
            estimatePriorityQueue.toArray(queueArray);

            for (int i = 0; i < 10; i++){
                topTen.add(queueArray[i].getMovieID());
            }
            for (int i = 1; i <= 10; i++){
                bottomTen.add(queueArray[estimatePriorityQueue.size()-i].getMovieID());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {}
            return rmse;
        }
    }

    public static void readMovieNames(){
        File dataFile = new File("u.item");
        BufferedReader reader = null;
        movieNames.clear();
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();

            while (line != null && line.length() != 0) {
                String[] splitLine = line.split("\\|");
                //Insert movieID --> movieName
                movieNames.put(Integer.parseInt(splitLine[0]), splitLine[1]);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    public static List<String> getMovieNames(List<Integer> movieIDList){
        List<String> listOfNames = new ArrayList<String>();
        for (Integer movieID : movieIDList){
            listOfNames.add(movieNames.get(movieID));
        }
        return listOfNames;
    }
	public static void main(String[] args){
		// readFile("ua.base");
  //       fillMoviesToUsers(); 
  //       getMovieAverages();
        
  //       //calculateAllMovieDistances();
  //       //predictMissingMovieRatings(0);
  //       System.out.println(getRMSE());
		// // for(Integer movie : predictions.keySet()){
		// // 	System.out.println("movie: "+movie+" rating: "+predictions.get(movie));
		// // }
		readFile("ua.base");
        readMovieNames();
        normalizeRatings();
        fillMoviesToUsers(); 
        getMovieAverages();
        
        final long startTime = System.currentTimeMillis();
        //calculateItemSimilarity();
        Double rmse = getRMSE();
        final long endTime = System.currentTimeMillis();
        System.out.println("Total execution time: " + (endTime - startTime)/1000);

        System.out.println("RMSE:"+rmse);
        System.out.println("Top 10: "+getMovieNames(topTen));
        System.out.println("Bottom 10: "+getMovieNames(bottomTen));
	}
}