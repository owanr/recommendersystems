import java.io.*;
import java.util.*;
/**
* ClusterPair.java is a class which stores the distance between two priority queues
* It extends comparable by comparing distance.
*
* @author Brynna Mering
*/
public class MovieEstimate implements Comparable<MovieEstimate>{
	private Integer movieID;
	private Double estimateDifference;

	/**
	* Constructor for ClusterPair
	*/
	public MovieEstimate(Integer id, Double diff){
		this.estimateDifference = diff;
		this.movieID = id;
	}

	/**
	* Constructor for ClusterPair
	*/
	public MovieEstimate(MovieEstimate movieEstimate){
		this.estimateDifference = movieEstimate.getDifference();
		this.movieID = movieEstimate.getMovieID();
	}

	public Double getDifference(){
		return this.estimateDifference;
	}

	public Integer getMovieID(){
		return this.movieID;
	}

	/**
	* compareTo() allows this ClusterPair to be compared to another ClusterPair based on distance
	*/

	public int compareTo(MovieEstimate compareMovieEstimate){
		if(compareMovieEstimate.getDifference()>this.estimateDifference){
			return -1;
		} else if(compareMovieEstimate.getDifference() == this.estimateDifference){
			return 0;
		} else {
			return 1;
		}
	}
}