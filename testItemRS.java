import java.io.*;
import java.util.*;
import java.lang.*;
/*
ItemRS, due 3/9
Written by Brynna Mering and Risako Owan
*/

public class testItemRS{
    //Dictionary of movie id -> set of users who have rated the movie
    private static HashMap<Integer, Set<Integer>> mapOfMoviesToUsers;
    //List of Dictionaries of movieId -> rating. The index of the dictionary is the user.
    private static List<HashMap<Integer, Integer>> listOfUserToRatings = new ArrayList<HashMap<Integer, Integer>>();
    //Dictionary containing the average rating for each movie
    private static HashMap<Integer, Double> movieAverages = new HashMap<Integer, Double>();
    //Dictionary containing the average rating given by each user
    private static HashMap<Integer, Double> userAverages = new HashMap<Integer, Double>();
    //List of Dictionaries of movieId -> normalized rating. The index of the dictionary is the user.
    private static List<HashMap<Integer, Double>> normalizedListOfUserToRatings = new ArrayList<HashMap<Integer, Double>>();

    private static HashMap<Integer, String> movieNames = new HashMap<Integer, String>();

    private static HashMap<Set<Integer>, Double> movieDistances = new HashMap<Set<Integer>, Double>();
    private static HashMap<Integer, PriorityQueue<ItemPair>> movieIDtoSimilarMovieQueue = new HashMap<Integer, PriorityQueue<ItemPair>>();

	private static List<Integer> topTen = new ArrayList<Integer>();
	private static List<Integer> bottomTen = new ArrayList<Integer>();
	private static int numSimilarMovies = 10;


    /*
    ** Reads data from file
    ** Fills listOfUserToRatings and userAverages (global variables)
    */
    public static void readFile(String fileName) {
        File dataFile = new File(fileName);
        BufferedReader reader = null;
        listOfUserToRatings.clear();
        userAverages.clear();
        try {
            //Initialize and declare variables
            Integer currentUser = 0;
            Double sum = 0.0;
            Integer n = 0;

            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            while (line != null && line.length() != 0) {
                String[] splitLine = line.split("\t");
                HashMap<Integer, Integer> movieRating = new HashMap<Integer, Integer>();
                //While we are getting ratings from the same user, put them in the same movieRating dictionary
                //Data user id | item id | rating | timestamp.
                while (currentUser == Integer.parseInt(splitLine[0])-1){
                    Integer movieID = Integer.parseInt(splitLine[1]);
                    Integer rating = Integer.parseInt(splitLine[2]);
                    movieRating.put(movieID,rating);
                    sum += rating;
                    n++;
                    line = reader.readLine();
                    if (line != null){
                        splitLine = line.split("\t");
                    } else {
                        break;
                    }
                }
                listOfUserToRatings.add(movieRating);
                userAverages.put(currentUser,(sum/n));
                currentUser++;
                sum = 0.0;
                n = 0;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    /*
    ** Normalizes the ratings and stores them in 
    ** normalizedListOfUserToRatings
    */
    public static void normalizeRatings(){
        normalizedListOfUserToRatings.clear();
        Integer userID = 0;
        for (HashMap<Integer, Integer> eachUserRating : listOfUserToRatings){
            HashMap<Integer, Double> temp = new HashMap<Integer, Double>();
            for (Integer movieID : eachUserRating.keySet()){
                temp.put(movieID,eachUserRating.get(movieID)-userAverages.get(userID));
            }
            normalizedListOfUserToRatings.add(temp);
            userID++;
        }
    }

    /*
    ** Fills mapOfMoviesToUsers
    */
    public static void fillMoviesToUsers(){
        //normalizeRatings();
        mapOfMoviesToUsers = new HashMap<Integer, Set<Integer>>();
        Set<Integer> movieAudience;
        //Loop through all users
        for(int i = 0; i<normalizedListOfUserToRatings.size(); i++){
            HashMap<Integer, Double> user = normalizedListOfUserToRatings.get(i);
            //For each movie that they rate, add that user to mapOfMoviesToUsers
            for(Integer movie : user.keySet()){
                if(mapOfMoviesToUsers.containsKey(movie)){
                    movieAudience = mapOfMoviesToUsers.get(movie);
                } else{
                    movieAudience = new HashSet<Integer>();
                }
                movieAudience.add(i);
                mapOfMoviesToUsers.put(movie, movieAudience);
            }
        }
    }

    /*
    ** Finds the average rating for each movie and stores it in movieAverages
    */
    public static void getMovieAverages(){
        for(Integer movieID : mapOfMoviesToUsers.keySet()){
            Double movieAvg = 0.0;
            int count = 0;
            Set<Integer> users = mapOfMoviesToUsers.get(movieID);
            for(Integer userID : users){
                movieAvg += normalizedListOfUserToRatings.get(userID).get(movieID);
                count++;
            }
            movieAvg = movieAvg/count;
            movieAverages.put(movieID, movieAvg);
        }
    }

    /*
    ** Calculates the similarity between two movies and returns a Double
    ** Gets called in predictMissingMovieRatings
    */
    public static void calculateItemSimilarity(){
        final long startTime = System.currentTimeMillis();
        Double similarity;
        for (int movie1 = 1; movie1 < 1682; movie1++){
            for (int movie2 = movie1+1; movie2 <= 1682; movie2++){
                Set<Integer> commonUsers = new HashSet<Integer>();
                //Some movies aren't in mapOfMoviesToUsers because they haven't been rated
            	if(mapOfMoviesToUsers.containsKey(movie1) && mapOfMoviesToUsers.containsKey(movie2)){
            		commonUsers.addAll(mapOfMoviesToUsers.get(movie1));
            		commonUsers.retainAll(mapOfMoviesToUsers.get(movie2));
            	} 
            	
            	List<Double> movie1Ratings = new ArrayList<Double>();
            	List<Double> movie2Ratings = new ArrayList<Double>();
                //System.out.print(commonUsers.size()+",  ");
                //If at least 2% of the population has seen both movies
                if(commonUsers.size() >= 20){
            	//if(!commonUsers.isEmpty()){
                    //Collects all the ratings that each movie has received from users who have seen both movies
        	    	for(Integer user : commonUsers){
        	    		HashMap<Integer, Double> userRating = normalizedListOfUserToRatings.get(user);
        	    		movie1Ratings.add(userRating.get(movie1));
        	    		movie2Ratings.add(userRating.get(movie2));
        	    	}

        	    	//Calculates dot product
        	    	Double dotProduct = 0.0;
        	    	Double vectorMagnitude1 = 0.0;
        	    	Double vectorMagnitude2 = 0.0;
        	    	for (int i = 0; i < commonUsers.size(); i++){
        	    		dotProduct += movie1Ratings.get(i)*movie2Ratings.get(i);
        	    		vectorMagnitude1 += Math.pow(movie1Ratings.get(i),2);
        	    		vectorMagnitude2 += Math.pow(movie2Ratings.get(i),2);
        	    	}
        	    	vectorMagnitude1 = Math.sqrt(vectorMagnitude1);
        	    	vectorMagnitude2 = Math.sqrt(vectorMagnitude2);

                    //final long endTime = System.currentTimeMillis();
                    //System.out.println("calculateItemSimilarity: " + (endTime - startTime)/1000);
        	    	similarity = (dotProduct/(vectorMagnitude1*vectorMagnitude2))-1;
        	    } else {
                    //final long endTime = System.currentTimeMillis();
                    //System.out.println("calculateItemSimilarity: " + (endTime - startTime)/1000);
                    //If no one has seen both, make them dissimilar
        	    	similarity = Double.MAX_VALUE;
        	    }//end of if-else

                if (similarity < Double.MAX_VALUE){
                    //Store for movie1
                    ItemPair itemPair1 = new ItemPair(movie1, movie2, similarity);
                    PriorityQueue<ItemPair> pq1;
                    if (movieIDtoSimilarMovieQueue.containsKey(movie1)){
                        pq1 = movieIDtoSimilarMovieQueue.get(movie1);
                    } else {
                        pq1 = new PriorityQueue<ItemPair>();
                    }
                    pq1.add(itemPair1);
                    movieIDtoSimilarMovieQueue.put(movie1,pq1);

                    //Store for movie2
                    ItemPair itemPair2 = new ItemPair(movie2, movie1, similarity);
                    PriorityQueue<ItemPair> pq2;
                    if (movieIDtoSimilarMovieQueue.containsKey(movie2)){
                        pq2 = movieIDtoSimilarMovieQueue.get(movie2);
                    } else {
                        pq2 = new PriorityQueue<ItemPair>();
                    }
                    pq2.add(itemPair2);
                    movieIDtoSimilarMovieQueue.put(movie2,pq2);
                }

            }
        }
    }

     /*
    ** Find the m items most similar to I.
    */
     public static Double getNMostSimilarMovieAvg(Integer givenMovieID, HashMap<Integer, Double> user){
        PriorityQueue<ItemPair> movieDistQueue = movieIDtoSimilarMovieQueue.get(givenMovieID);
        Double average = 0.0;
        Double num = 0.0;

        if (movieDistQueue != null && movieDistQueue.size() > numSimilarMovies){
            for (int i = 0; i < numSimilarMovies; i++){
                //ItemPair simMovie = movieDistQueue.poll();
                Integer simMovieID = movieDistQueue.poll().getItem2();
                //nMostSimilarMovies.add(simMovieID);
                if (user.containsKey(simMovieID)){
                    average += user.get(simMovieID);
                    num++;
                }

            }
        }
        if (num > 0.0){
            return (average/num);
        } else {
            return 0.0;
        }
     }

    /*
    ** Take the average rating among the m items, of the ratings that U has given.
    ** And uses it as the prediction
    */
    public static Double predictMissingMovieRatingForGivenUser(HashMap<Integer, Double> user, Integer missingMovie){
        return getNMostSimilarMovieAvg(missingMovie, user);
    }

    public static Double getRMSE(){
        //Polls smallest first
        PriorityQueue<MovieEstimate> estimatePriorityQueue = new PriorityQueue<MovieEstimate>();
        File dataFile = new File("ua.test");
        BufferedReader reader = null;
        Double rmse = 0.0;
        //Each user has 10 movies in the test data
        Integer numEstimated = 10*normalizedListOfUserToRatings.size();
        HashMap<Integer, Double[]> movieIDtoRatingDiff = new HashMap<Integer, Double[]>();
            //movieId --> [diff, numRated]
        try {
            Integer currentUser = 0;
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            while (line != null && line.length() != 0) {
            	String[] splitLine = line.split("\t");
                while (currentUser == Integer.parseInt(splitLine[0])-1){
                	Integer movieID = Integer.parseInt(splitLine[1]);
	                Double normalizedActualRating = Double.parseDouble(splitLine[2])-userAverages.get(currentUser);	                
                    Double estimatedRating = predictMissingMovieRatingForGivenUser(normalizedListOfUserToRatings.get(currentUser),movieID); //estimateRatingForGivenUser.get(movieID);
	                Double ratingDiff = normalizedActualRating - estimatedRating;
                    rmse += Math.pow(ratingDiff,2);
                    Double[] movieInfo;
                    if (movieIDtoRatingDiff.containsKey(movieID)){
                        movieInfo = movieIDtoRatingDiff.get(movieID);
                        movieInfo[0] = movieInfo[0] + Math.pow(ratingDiff,2);
                        movieInfo[1] = movieInfo[1] + 1;
                    } else {
                        movieInfo = new Double[2];
                        movieInfo[0] = Math.pow(ratingDiff,2);
                        movieInfo[1] = 1.0;
                    }
                    movieIDtoRatingDiff.put(movieID, movieInfo);
                    line = reader.readLine();
                    if (line != null){

                        splitLine = line.split("\t");
                    } else {
                        break;    
                    }
                }//end of while-loop
                currentUser++;
            }
            for (Integer movieIDForPQ : movieIDtoRatingDiff.keySet()){
                Double[] ratingDiffInfo = movieIDtoRatingDiff.get(movieIDForPQ);
                //Checking how many times you guessed; ratingDiffInfo[1] = numberOfUsersRated
                //How we chose parameter 30:
                //The max was for common number of users was 88, so we wanted something near 40
                //With 30, the size of the PQ becomes 39, so getting the top/bottom 10 gives us the first quartile and last quartile
                if (ratingDiffInfo[1] > 30){
                    MovieEstimate movieEstDiff = new MovieEstimate(movieIDForPQ, ratingDiffInfo[0]);
                    estimatePriorityQueue.add(movieEstDiff);
                }
            }
            rmse = Math.sqrt((rmse/numEstimated));
            
            //Converting queue to array to get the 10 best and 10 worst
            MovieEstimate[] queueArray = new MovieEstimate[estimatePriorityQueue.size()];
            queueArray = estimatePriorityQueue.toArray(queueArray);
            //Gets the top 10 best and worst estimates
            for (int i = 0; i < 10; i++){
                topTen.add(queueArray[i].getMovieID());
                bottomTen.add(queueArray[estimatePriorityQueue.size()-1-i].getMovieID());
            }
        //Converting queue to array to get the 10 best and 10 worst
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {}
            return rmse;
        }
    }

    public static void readMovieNames(){
        File dataFile = new File("u.item");
        BufferedReader reader = null;
        movieNames.clear();
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();

            while (line != null && line.length() != 0) {
                String[] splitLine = line.split("\\|");
                //Insert movieID --> movieName
                movieNames.put(Integer.parseInt(splitLine[0]), splitLine[1]);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    public static List<String> getMovieNames(List<Integer> movieIDList){
        List<String> listOfNames = new ArrayList<String>();
        for (Integer movieID : movieIDList){
            listOfNames.add(movieNames.get(movieID));
        }
        return listOfNames;
    }

    public static void main(String[] args){
        readFile("ua.base");
        readMovieNames();
        normalizeRatings();
        fillMoviesToUsers(); 
        getMovieAverages();
        calculateItemSimilarity();
        
        Double rmse = getRMSE();

        System.out.println("RMSE:"+rmse);
        System.out.println("Top 10: "+getMovieNames(topTen));
        System.out.println("Bottom 10: "+getMovieNames(bottomTen));
    }
}