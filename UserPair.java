import java.io.*;
import java.util.*;
/**
* ClusterPair.java is a class which stores the distance between two priority queues
* It extends comparable by comparing distance.
*
* @author Brynna Mering
*/
public class UserPair implements Comparable<UserPair>{
	private Integer user1;
	private Integer user2;
	private Double distance;

	/**
	* Constructor for ClusterPair
	*/
	public UserPair(Integer u1, Integer u2, Double distance){
		this.distance = distance;
		this.user1 = u1;
		this.user2 = u2;
	}

	/**
	* Constructor for ClusterPair
	*/
	public UserPair(UserPair userPair){
		this.distance = userPair.getDistance();
		this.user1 = userPair.getUser1();
		this.user2 = userPair.getUser2();
	}

	public Double getDistance(){
		return this.distance;
	}

	public Integer getUser1(){
		return this.user1;
	}

	public Integer getUser2(){
		return this.user2;
	}

	/**
	* compareTo() allows this ClusterPair to be compared to another ClusterPair based on distance
	*/

	public int compareTo(UserPair compareUserPair){
		if(compareUserPair.getDistance()>this.distance){
			return -1;
		} else if(compareUserPair.getDistance() == this.distance){
			return 0;
		} else {
			return 1;
		}
	}
}